package ProjectPack;

import ProjectPack.TrainingPack.TrainingApp;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {

        TrainingApp TA = new TrainingApp();

        TA.RUN();
    }
}
