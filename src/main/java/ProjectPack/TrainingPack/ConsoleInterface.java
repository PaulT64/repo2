package ProjectPack.TrainingPack;

import ProjectPack.TrainingPack.TrainingTasksPack.Task;

import java.util.ArrayList;
import java.util.Scanner;

public class ConsoleInterface {
    private ArrayList<Task> TasksArr;
    private ArrayList<String> TaskName;
    private Scanner scanner;
    private String string;

    public ConsoleInterface(ArrayList<Task> Arr1, ArrayList<String> Arr2, Scanner scanner1){
        this.TasksArr = Arr1;
        this.TaskName = Arr2;
        this.scanner = scanner1;
    }

    public void run(){

        while(true){
            for (int i=0; i<30; i++){
                System.out.println("\n");
            }

            System.out.println("Выбери задание:");
            for (int i = 0; i < TaskName.size(); i++) {
                System.out.println(i + 1 + " - Задание " + TaskName.get(i));
            }
            System.out.println("quit - Выход");
            System.out.print(">>> ");

            string = scanner.nextLine();
            if (string.equals("")){
                continue;
            }
            if (string.equals("quit")) {
                break;
            }else {
                System.out.println("Задание " + string + "\n");
                TasksArr.get(Integer.parseInt(string)-1).run();
                string = scanner.nextLine();
                if (string.equals("")){
                    continue;
                }
            }
        }
    }


}
