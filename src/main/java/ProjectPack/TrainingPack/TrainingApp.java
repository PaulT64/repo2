package ProjectPack.TrainingPack;

import ProjectPack.TrainingPack.TrainingTasksPack.Task;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Scanner;

public class TrainingApp {
    private ArrayList<Task> TasksArr = new ArrayList<Task>(); //массив выполненыых заданий
    private ArrayList<String> TaskName = new ArrayList<String>();

    public void RUN() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

        String PathToClass = "./src/main/java/ProjectPack/TrainingPack/TrainingTasksPack/TasksPack/";
        String ClassName;

        File folder = new File(PathToClass);
        File[] listOfFiles = folder.listFiles();

        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                ClassName = listOfFiles[i].getName().substring(0, listOfFiles[i].getName().indexOf("."));
                TaskName.add(listOfFiles[i].getName().substring(listOfFiles[i].getName().indexOf("_"),listOfFiles[i].getName().indexOf(".")));
                TasksArr.add((Task) Class.forName("ProjectPack.TrainingPack.TrainingTasksPack.TasksPack."+ClassName).getConstructor().newInstance());
            } else if (listOfFiles[i].isDirectory()) {}
        }

        ConsoleInterface CI = new ConsoleInterface(TasksArr,TaskName,sc);
        CI.run();

    }
}
