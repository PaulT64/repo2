package ProjectPack.TrainingPack.TrainingTasksPack;

import java.util.Scanner;

public abstract class Task {
    protected Scanner sc = new Scanner(System.in);
    //класс от которого будут наследоваться все другие классы с заданиями
    public static void Task(){}

    public abstract void run();

}
