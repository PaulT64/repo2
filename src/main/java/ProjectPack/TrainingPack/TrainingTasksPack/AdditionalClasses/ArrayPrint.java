package ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses;

public class ArrayPrint {
    public static void print(int[][] arr)//вывод двумерного массива на экран
    {
        //arr = new int[m][n];
        for (int i=0; i<arr.length; i++){
            for(int j=0; j<arr[0].length; j++){
                System.out.print(arr[i][j]+"|");
            }
            System.out.println("");

        }

    }
    public static void print(int[] arr)//перегрузка метода, для вывода одномерного массива
    {
        //arr = new int[m][n];
            for(int i=0; i<arr.length; i++){
                System.out.print(arr[i]+"|");
            }

    }
}
