package ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses;

import java.util.Random;

public class RandomArr {
    public static int[][] run(int[][] arr,int m,int n)
    {   Random r = new Random();


        for(int i =0; i<m; i++)
        {
            for(int j =0; j<n; j++)
            {
                arr[i][j] = r.nextInt((100));
            }
        }
        return arr;
    }
}
