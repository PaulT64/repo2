package ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses;

public class PalindromeCheck //проверка, является ли введенна строка палиндромом
{
    public static boolean run(String str){
        int i = str.length() -1;
        int j = 0;
        while(i > j){
            if(str.charAt(i) != str.charAt(j)){
                return false;
            }
            i--;
            j++;
        }

        return true;
    }
    }

