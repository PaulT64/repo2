package ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses;

public class NumberToDigits {
    public static int[] run(int a)
    {
        char[] arrChar = Integer.toString(a).toCharArray();
        int[] arrInt = new int[arrChar.length];
        for (int i=0; i<arrChar.length; i++){
            arrInt[i] = Character.getNumericValue(arrChar[i]);
        }
        return arrInt;


    }
}
