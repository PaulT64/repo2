package ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses;

import java.util.Scanner;
public class Scan {
    static Scanner sc = new Scanner(System.in);

    public static int In(){int a = sc.nextInt(); sc.close();
    return a;}//сканирует int

    public static double Do(){double a = sc.nextDouble(); sc.close();
    return a;}//сканирует double

    public static String St(){String a =sc.nextLine(); sc.close();
    return a;}//всю строку

    public static Character Ch(){String a =sc.next("."); sc.close();
        char c = a.charAt(0);
        return c;}//Метод считывающий один символ типа Char
    /*-------------------------------------------------------------------------*/

    public static boolean IsInt(){boolean a = sc.hasNextInt(); sc.close();
        return a;}//Проверка типов
    public static boolean IsDouble(){boolean a = sc.hasNextDouble(); sc.close();
        return a;}//Проверка типов
    public static boolean IsString(){boolean a = sc.hasNextLine(); sc.close();
        return a;}//Проверка типов
    public static boolean IsChar(){boolean a = sc.hasNext(); sc.close();
        return a;}//Проверка типов
}
