package ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses;

public class ArrayTranspose {
    public static void run(int[][] arr){
        for (int i = 0; i < arr[0].length; i++) {
            for (int j = i+1; j < arr.length; j++) {
                int temp = arr[i][j];
                arr[i][j] = arr[j][i];
                arr[j][i] = temp;
            }
        }
    }
}
