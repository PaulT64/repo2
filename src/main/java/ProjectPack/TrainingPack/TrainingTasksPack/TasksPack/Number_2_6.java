/*6. Разработайте программу, которая будет выводить таблицу умножения введенного пользователем числа с клавиатуры.*/
package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.Scan;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

public class Number_2_6 extends Task {
    public  void run(){
        System.out.println("Введите число, для которого хотите получить таблицу умножения");
        double a;
        a = super.sc.nextDouble();
        table(a);

    }
    public  static void table(double a) //таблица умножения для вещественного числа
    {
        for (int b = 1; b <= 10; b++) {
            double result = a * b;
            System.out.println(a + " * " + b + " = " + result);
        }
    }
}
