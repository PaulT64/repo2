/*Напишите программу, которая будет считывать с консоли число
и выводить его в таком виде:*/
package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.NumberToDigits;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

public class Number_3_2 extends Task {
    @Override
    public void run() {
        System.out.println("Введите число");
        if (super.sc.hasNextInt()) {
            int a = super.sc.nextInt();
            int[] arrInt = NumberToDigits.run(a);

            for (int line = 0; line < 7; line++) {
                for (int j = 0; j < arrInt.length; j++) {
                    System.out.print(view[line][arrInt[j]]);
                }
                System.out.println();
            }


        } else {
            System.out.println("Неверный ввод");
        }
    }


    private static final String[][] view = { //Это жесть, надо это осознать...

            {"  ***  ", "   *   ", "  ***  ", "  ***  ", "    *  ", " ***** ", "  ***  ", " ***** ", "  ***  ", "  ***  "},
            {" *   * ", "  **   ", " *   * ", " *   * ", "   **  ", " *     ", " *   * ", "     * ", " *   * ", " *   * "},
            {"*     *", " * *   ", " *   * ", "     * ", "  * *  ", " *     ", " *     ", "    *  ", " *   * ", " *   * "},
            {"*     *", "   *   ", "    *  ", "  ***  ", " *  *  ", " ****  ", " ****  ", "   *   ", "  ***  ", "  ***  "},
            {"*     *", "   *   ", "   *   ", "     * ", " ***** ", "     * ", " *   * ", "  *    ", " *   * ", "     * "},
            {" *   * ", "   *   ", "  *    ", " *   * ", "    *  ", " *   * ", " *   * ", "  *    ", " *   * ", "     * "},
            {"  ***  ", " ***** ", " ***** ", "  ***  ", "    *  ", "  ***  ", "  ***  ", "  *    ", "  ***  ", "  ***  "}};
}
