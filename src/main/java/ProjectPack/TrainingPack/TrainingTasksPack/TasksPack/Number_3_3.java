/*3. Определить является ли символ введенный с клавиатуры цифрой,
 буквой или знаком пунктуации.*/
package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.Task;

public class Number_3_3 extends Task {
    @Override
    public void run() {
        System.out.println("Введите что нибудь с клавиатуры");
        char c = super.sc.next().charAt(0);
        if (Character.isDigit(c)) {
            System.out.println("Цифра");
        } else if (Character.isLetter(c)) {
            System.out.println("Буква");
        } else if (",.:!'".indexOf(c) > -1) {
            System.out.println("Знак препинания");
        } else {
            System.out.println("Что-то не то");
        }

    }
}