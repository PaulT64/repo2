package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;/* Расчет расстояния до места удара молнии.
Звук в воздухе распространяется со скоростью приблизительно равной 1100 футам в секунду.
Зная интервал времени между вспышкой молнии и звуком сопровождающим ее можно рассчитать расстояние.
Допустим интервал 7,2 секунды.*/

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.Scan;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

public class Number_1_5 extends Task {
    @Override
    public void run(){
        System.out.println("Введите интервал времени от момента удара молнии");
        double t = super.sc.nextDouble();
        double distance = 343 * t;
        System.out.println("Молния ударила в "+distance+" метрах");
    }
}
