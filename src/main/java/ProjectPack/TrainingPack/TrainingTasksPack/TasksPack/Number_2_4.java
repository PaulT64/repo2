/*4. Создайте метод, который будет считать сколько денег получает работник в неделю.
Метод должен принимать на входе два аргумента (зарплата в час, кол-во проработанных часов).
Условия:
1) Каждый час после 40 считается за полтора.
2) Работник не может работать больше, чем 60 часов в неделю.
3) Работник не может получать меньше 8 долларов в час.*/

package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.Scan;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

public class Number_2_4 extends Task {
    public void run() {

        double m;
        double time;
        double temp;

        System.out.println("Введите зарплату в час, и колличество часов в неделю");
        m = super.sc.nextDouble();

        if(m<8)
            System.out.println("Работник не может получать меньше 8 долларов в час.");
        else
            {
            time = super.sc.nextDouble();
            if (time >= 40)
            {
                temp = (time - 40) * 1.5;
                time += temp;

                if (time >60)
                    System.out.println("Работник не может работать больше, чем 60 часов в неделю.");
                else
                    System.out.println("Зарплата работника за неделю составила" + m*time);
            } else
                System.out.println("Зарплата работника за неделю составила" + m*time);
        }

    }
}
