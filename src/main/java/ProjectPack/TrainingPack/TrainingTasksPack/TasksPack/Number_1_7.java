package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;//7. Напишите программу, которая вычислит простые числа в пределах от 2 до 100.
//(Опционально) Дополните программу так, чтобы она вычисляла составные числа.
import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.Scan;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

import java.util.Arrays;
public class Number_1_7 extends Task {

    @Override
    public void run() {
        System.out.println("Введите размер N");
        int N = super.sc.nextInt();
        boolean[] isPrime;
        isPrime = new boolean[N];//булевский массив, для решета
        int[] arrS;//массив простых чисел
        int[] arrC;//массив составных чисел
        int S = 0;
        int C = 0;
        for (int i = 0; i < N; i++) isPrime[i] = true;//заполняем булевский массив значениями true

        isPrime = EratosphenesSieve(isPrime, N);//Применяем метод
        S = counter(isPrime, S, N);//Считаем колличество вхождений true в массив
        arrS = new int[S];//Создаем массив размера S, для простых чисел
        arrS = simpledigits(isPrime, S, N, arrS);//Заполняем его
        C=N-S;
        arrC = new int[C];
        arrC = compdigits(isPrime, C, N, arrC);//массив составных чисел
        System.out.println("Простые числа:");
        for (int i = 1; i < S; i++) System.out.print(arrS[i]+" ");
        System.out.println("");
        System.out.println("Составные числа");
        for (int i =0 ; i < C; i++) System.out.print(arrC[i]+" ");

    }

    public static boolean[] EratosphenesSieve(boolean isPrime[], int N) //Метод решето Эратосфена
    {
        Arrays.fill(isPrime, true);
        isPrime[1] = false;
        for (int i = 2; i * i < N; i++)
            if (isPrime[i])
                for (int j = i * i; j < N; j += i)
                    isPrime[j] = false;
        return isPrime;
    }
    public static int counter(boolean[] isPrime, int S, int N) //Считает колличество простых чисел(true в isPrime)
    {
        for (int i = 0; i < N; i++) {
            if (isPrime[i] == true) {
                S = S + 1;
            }
        }
        return S;
    }

    public static int[] simpledigits(boolean[] isPrime, int S, int N, int arrS[]) //Заполняет массив простых чисел
    {
        int j = 0;
        for (int i = 0; i < N; i++)
        {
            if (isPrime[i] == true)
            {
                arrS[j] = i;
                j++;
            }
        }
        return arrS;
    }
        public static int[] compdigits(boolean[] isPrime, int C, int N, int arrC[])//Заполняет массив составных чисел
        {
            int j=0;
            for (int i = 0; i < N; i++)
            {
                if (isPrime[i] == false)
                {
                    arrC[j] = i;
                    j++;
                }
            }

        return arrC;

    }
}
//Переделать с методом двойного решета, чтобы можно было указывать диапазон рассчета [a,b]