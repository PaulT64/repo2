package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.Scan;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

public class Number_1_9 extends Task {

    @Override
    public void run()
    {
     System.out.println("Введите строку:");
      String s = super.sc.nextLine();
        if(s.length() > 0) {
            reverseString(s, s.length() - 1);
        }
    }

    public static void reverseString(String s, int i) {
        if(i == 0) {
            System.out.println(s.charAt(i));

            return;
        }

        System.out.print(s.charAt(i));

        reverseString(s, i - 1);
    }
}
