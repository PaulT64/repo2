/*7. Напишите программу, которая будет просить ввести ваше имя,
 а потом выведет его на консоль.*/
package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.Scan;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

public class Number_2_7 extends Task {
    @Override
    public void run()
    {
        System.out.println("Введите ФИО жителя");
        String a = super.sc.nextLine();
        System.out.println("Гражданин "+a+" уже покинул убежище 101");
    }
}
