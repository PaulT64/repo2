/*4. Пользователь вводит задает расстояние до места назначения (N) и время,
 за которое нужно доехать (T). Вычислить скорость (км/ч), с которой нужно ехать.*/
package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.Task;

public class Number_3_4 extends Task {
    @Override
    public void run() {
        System.out.println("Введите расстояние(км) до пункта N и требуемое время(час)");
       double a = super.sc.nextDouble();
       double b = super.sc.nextDouble();
       System.out.println("Средняя скорость =" +(a/b));
    }
}
