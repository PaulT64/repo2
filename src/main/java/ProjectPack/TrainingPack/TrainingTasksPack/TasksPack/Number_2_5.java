/*5. Напишите метод, который будет проверять является ли число палиндромом
(одинаково читающееся в обоих направлениях).
(Опционально) усовершенствуйте метод для проверки символьной строки.*/
package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.PalindromeCheck;
import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.Scan;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

public class Number_2_5 extends Task {
    public void run(){
        System.out.println("Введите строку которую хотите проверить");
        String str = super.sc.nextLine();
        boolean check;
        check = PalindromeCheck.run(str);
        if(check == true) System.out.println("Палиндром");
        else System.out.println("Не палиндром");
    }

}
