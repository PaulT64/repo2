/*5. Напишите метод переводящий рубли в доллары по заданному курсу.
В качестве аргументов передайте кол-во рублей и курс.*/
package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.Task;

public class Number_3_5 extends Task {
    @Override
    public void run() {
        System.out.println("Введите колличество рублей и курс");
       double a = super.sc.nextDouble();
       double b = super.sc.nextDouble();
       double c = exchange(a,b);
       System.out.println(c+" долларов");

    }
    private double exchange(double a, double b)
    {
        double c = a/b;
        return c;
    }
}
