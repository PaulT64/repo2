/*9. У вас есть двухмерный массив [n][n], придумайте способ поменять столбцы и строки местами.
//Переделать, для матриц произвольного размера*/
package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.ArrayPrint;
import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.ArrayTranspose;
import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.RandomArr;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

import java.util.Scanner;

public class Number_2_9 extends Task {
    @Override
    public void run() {
        System.out.println("Введите размер массива n*n");
        //int m = sc.nextInt();
        int n = super.sc.nextInt();
        int[][] arr = new int[n][n];
        //int[][] arrT = new int[n][m];
            System.out.println("Исходная матрица");
            RandomArr.run(arr, n, n);
            ArrayPrint.print(arr);
            System.out.println("Транспонированная матрица");
            ArrayTranspose.run(arr);
            ArrayPrint.print(arr);
        }
    }
