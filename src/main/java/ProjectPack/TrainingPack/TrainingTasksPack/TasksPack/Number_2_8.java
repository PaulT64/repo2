/*8. У вас есть двухмерный массив наполненный случайными числами,
в той же последовательности перенесите эти числа в одномерный массив.*/
package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.ArrayPrint;
import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.RandomArr;
import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.TwoDToOneD;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

public class Number_2_8 extends Task {
    public void run(){
        int arr[][];
        System.out.println("Введите размер массива m*n");
        int m = super.sc.nextInt();
        int n = super.sc.nextInt();
        arr = new int[m][n];
        RandomArr.run(arr,m,n);
        ArrayPrint.print(arr);
        System.out.println("\n Массив-строка \n");
        ArrayPrint.print(TwoDToOneD.run(arr));
    }
}
