package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;/*6. Создайте простую игру основанную на угадывании букв.
Пользователь должен угадать загаданную букву A-Z введя ее в консоль.
Если пользователь угадал букву программа выведет «Right» и игра закончится, если нет,
то пользователь продолжит вводить буквы.
(Опционально) Вывести «You’re too low»- если пользователь ввел букву меньше загаданной,
 «You’re too high»- если пользователь ввел букву больше загаданной.*/

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.Scan;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

import java.util.Random;

public class Number_1_6 extends Task {
    @Override
    public void run() {
        Random r = new Random();
        char q = (char)(r.nextInt(26)+'a');
       // char q = 't';//test
        String a;
        int i = 0;
        System.out.println("Угадайте загаданную букву алфавита a-z.");
        do {
            a = super.sc.nextLine();
            i++;
            if (a.equals(q)) {
                System.out.println("Верно!");
                System.out.println("Колличество попыток:=" + i);
                break;
            } else {
                System.out.println("Попробуйте ещё раз.");
            if (Character.getNumericValue(Integer.parseInt(a)) + 1 == Character.getNumericValue(q))
                System.out.println("/Подсказка/ следующая буква");
            if (Character.getNumericValue(Integer.parseInt(a)) - 1 == Character.getNumericValue(q))
                System.out.println("/Подсказка/ предыдущая буква");
            }
        } while (a.equals(q) == false);


    }
}
