package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.Scan;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

//8. Найдите среднее арифметическое массива из 10 элементов типа double.
public class Number_1_8 extends Task {

    @Override
    public void run(){
        double arr[];
        arr = new double[10];
        System.out.println("Заполните массив из 10 элементов:");
        for (int i=0; i<10; i++){
            arr[i]= super.sc.nextDouble();
        }
        int S=0;
        for (int i=0; i<10; i++){
            S+= arr[i];
        }
        System.out.println("Среднее арифметическое:="+S/10);
    }
}
