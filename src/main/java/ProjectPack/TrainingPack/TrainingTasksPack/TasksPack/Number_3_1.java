/*1. Придумайте способ превращения числа, в массив из его разрядов.  Пример: 562 -> [5,6,2].*/
package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.ArrayPrint;
import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.NumberToDigits;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

import java.util.Scanner;

public class Number_3_1 extends Task {
    @Override
    public void run() {
        System.out.println("Введите число");
       if(super.sc.hasNextInt()){
           int a = super.sc.nextInt();
          int[] arrInt = NumberToDigits.run(a);
           ArrayPrint.print(arrInt);

       } else {System.out.println("Неверный ввод");}
    }
}
