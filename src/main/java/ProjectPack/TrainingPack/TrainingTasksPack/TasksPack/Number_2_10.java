/*10. Напишите программу, которая будет считать количество часов,
минут и секунд в n-ном количестве суток.*/
package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.Task;

import java.util.Scanner;

public class Number_2_10 extends Task {
    @Override
    public void run() {
        System.out.println("Введите колличество суток");
        int n = super.sc.nextInt();
        System.out.println("В "+n+" суток:\n"+(n*24)+" часов.\n"+(n*24*60)+" минут.\n"+(n*24*60*60)+" секунд.");

    }
}
