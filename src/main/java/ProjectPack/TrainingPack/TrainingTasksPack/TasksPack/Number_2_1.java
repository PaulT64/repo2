package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.Task;

import java.util.Scanner;
public class Number_2_1 extends Task {

    @Override
    public void run(){
        System.out.println("Введите число");
        if(super.sc.hasNextDouble()){
        double a = sc.nextDouble();

        if (a%1 == 0) System.out.println("Число целое");
        else System.out.println("Число вещественное");}
        else System.out.println("Вы ввели не число");
    }
}
