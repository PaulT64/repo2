package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.Scan;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

/*Напишите метод, заменяющий в строке все вхождения слова «бяка» на «вырезано цензурой»*/
public class Number_1_4 extends Task {
    @Override
    public void run(){
        System.out.println("Введите строку");
        String str1 = super.sc.nextLine();
        String str2 = str1.replaceAll("хуй", "/вырезанно цензурой/");
        System.out.println(str2);
    }
}
