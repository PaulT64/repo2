package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.Scan;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

/*2. Сила тяжести на Луне примерно равна 17%, напишите программу, которая вычисляет ваш вес на Луне.*/
public class Number_1_2 extends Task {
    @Override
    public void run(){
        System.out.println("Введите свой вес: m1= ");

        double m1;
        m1 = super.sc.nextDouble();
        double m2= m1/6;
        System.out.println("Ваш вес на луне: m2="+m2);

    }
}
