/*2. Создайте метод, который в качестве аргумента получает число
и полностью обнуляет столбец, который соответствует заданному числу.*/
package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.ArrayPrint;
import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.ColumnZeroing;
import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.RandomArr;
import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.Scan;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

public class Number_2_2 extends Task {
    @Override
    public  void run(){
        System.out.println("Введите размер массива m*n");
        int m = super.sc.nextInt();
        int n = super.sc.nextInt();
        int[][] arr = new int[m][n];
        RandomArr.run(arr,m,n);
        ArrayPrint.print(arr);
        System.out.println("Введите номер столбца который хотите обнулить");
        int index = super.sc.nextInt();
        ColumnZeroing.run(arr,index,m,n);
        ArrayPrint.print(arr);
    }
}
