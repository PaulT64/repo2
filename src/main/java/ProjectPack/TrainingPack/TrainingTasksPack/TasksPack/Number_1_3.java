package ProjectPack.TrainingPack.TrainingTasksPack.TasksPack;

import ProjectPack.TrainingPack.TrainingTasksPack.AdditionalClasses.Scan;
import ProjectPack.TrainingPack.TrainingTasksPack.Task;

/* Напишите метод, который будет увеличивать заданный элемент массива на 10%.*/
public class Number_1_3 extends Task {
    @Override
    public void run(){
        double[] arr = new double[10];
        int i;
        System.out.print("Заполните массив из 10 элементов:");
        for (i=0; i<10; i++){
            System.out.print("["+(i+1)+"]=");
            arr[i] = super.sc.nextDouble();
        }
        System.out.println("Введите номер элемента который хотите увеличить");
        int n = super.sc.nextInt() - 1;
        System.out.println(arr[n]*1.1);


    }
}
//Сделать с динамическим массивом, с вводом размера.